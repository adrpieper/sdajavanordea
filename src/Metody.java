
public class Metody {

	public static void main(String[] args) {
		dodajIWyswietl(10, 4);
		int a = dodaj(20,9);
		System.out.println(a);
		System.out.println(dodaj(30,5));
		System.out.println(pomnoz(30,5));
		int b = mniejsza(30,5);
		System.out.println(b);

		System.out.println(mniejsza(1, 3, 6));
		System.out.println(mniejsza(4, 2, 6));
		System.out.println(mniejsza(5, 3, 1));
		int[] tab = {12,2,3,2,10};
		System.out.println(najmniejsza(tab));
		System.out.println(najmniejsza(new int[]{12,12,32,12,1,2}));
	}
	
	private static void dodajIWyswietl(int a,int b) {
		int wynik = a + b;
		System.out.println(wynik);
	}
	
	private static int dodaj(int a, int b) {
		int wynik = a + b;
		return a+b;
	}
	
	private static int pomnoz(int a, int b) {
		int wynik = a * b;
		return wynik;
	}
	
	private static int mniejsza(int a, int b) {
		if (a>b) {
			return b;
		}else {
			return a;
		}
	}
	
	private static int najmniejsza(int[] liczby) {
//		Kod r�wnowa�nych (oba ify robi� to samo
//		Przyk�ad 1:
//		if (liczby == null || liczby.length == 0) {
//			return Integer.MAX_VALUE;
//		}

//		Przyk�ad 2:
//		if (liczby == null) {
//			return Integer.MAX_VALUE;
//		}else {
//			if (liczby.length == 0) {
//				return Integer.MAX_VALUE;
//			}
//		}
		
		
		//Sprawczam czy tablica istnieje i czy nie jest pusta
		if (liczby == null || liczby.length == 0) {
			return Integer.MAX_VALUE;
		}
		
		//Tu trzeba zwr�ci� konkretny wynik
		
		int wynik = liczby[0];
		
		for(int i = 1; i < liczby.length ; i++) {
			if (liczby[i] < wynik) {
				wynik = liczby[i];
			}
		}
		return wynik;
	}
	
	private static int mniejsza(int a, int b, int c) {
		return mniejsza(a , mniejsza (b, c));
	}

}
