package osoba;

public class OsobaProgram {

	public static void main(String[] args) {

		Osoba jan = new Osoba("Jan", 20);
		System.out.println(jan.getImie());
		
		Object o = jan;
		System.out.println(o.toString());
		
		Osoba[] osoby = new Osoba[4];
		osoby[0] = new Osoba("Jan", 20);
		osoby[1] = new Osoba("Damian", 22);
		osoby[2] = new Osoba("Kasia", 24);
		osoby[3] = new Osoba("Asia", 25);
		
		for (Osoba osoba : osoby) {
			System.out.println(osoba);
		}
		
	}

}
