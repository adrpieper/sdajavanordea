package osoba;

public class Osoba {
	private String imie = "?";
	private int wiek = 1;
	
	public Osoba() {
	
	}
	
	public Osoba(String imie, int wiek) {
		this.imie = imie;
		this.wiek = wiek;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}
	
	public void setWiek(int wiek) {
		this.wiek = wiek;
	}
	
	public String getImie() {
		return imie;
	}
	
	public int getWiek() {
		return wiek;
	}
	
	public String toString() {
		return imie + " (" + wiek + ")";
	}
}
