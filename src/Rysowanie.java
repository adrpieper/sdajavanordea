
public class Rysowanie {

	public static void main(String[] args) {
		System.out.println("Prostok�t: ");
		rysujProstokat(2, 12);
		System.out.println("Tr�jkat 5x5: ");
		rysujTrojkat(5);
		System.out.println("Tr�jkat 10x5: ");
		rysujTrojkat(10, 5);
		System.out.println("Tr�jkat 5x10: ");
		rysujTrojkat(5, 10);
	}

	private static void rysujTrojkat(int a) {
		for(int i = 1 ; i <= a ; i++) {
			rysujLinie(i);
		}
	}

	private static void rysujTrojkat(int a, int b) {
		for(int i = 1 ; i <= a ; i++) {
			rysujLinie(i*b/a);
		}
	}
	
	private static void rysujProstokat(int a,int b) {
		for(int i = 0 ; i < a ; i++) {
			rysujLinie(b);
		}
	}

	private static void rysujLinie(int a) {
		for(int i = 0 ; i < a ; i++) {
			System.out.print('*');
		}
		System.out.println();
	}
}
