package animals;

public interface Animal {
	// Metoda jest abstrakcyjna i publiczna z definicji
	// bo znajduje si� w interfejsie
	void makeNoise(); 
}
