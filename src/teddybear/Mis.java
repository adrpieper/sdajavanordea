package teddybear;

public class Mis {
	private String imie;

	public Mis(String s) {
		this.imie = s;
	}
	
	public String getImie() {
		return this.imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}
	
	public void przedstawSie() {
		System.out.println("Jestem misiem o imieniu " + this.imie);
	}
}
