
public class SumaLiczb {

	public static void main(String[] args) {
		sumaLiczb();
		iloczynLiczb();
		silnia(5);
		sumaCiaguArytmetycznego();
	}

	private static void sumaCiaguArytmetycznego() {
		int a0 = 10;
		int r = 2;
		int n = 5;
		int suma = 0;
		for(int i = 0 ; i < n ; i++) {
			suma += a0 + r*i;
		}
		System.out.println(suma);
	}

	private static void sumaLiczb() {
		int[] liczby = { 1, 2, 3, 4};
		
		int wynik = 0;
		for(int liczba : liczby) {
			wynik += liczba;
		}
		System.out.println(wynik);
	}
	
	private static void iloczynLiczb() {
		int[] liczby = { 1, 2, 3, 4};
		
		int wynik = 1;
		for(int liczba : liczby) {
			wynik *= liczba;
		}
		System.out.println(wynik);
	}
	
	private static void silnia(int n) {
		
		int wynik = 1;
		for(int i = 2 ; i <= n ; i++) {
			wynik *= i;
		}
		System.out.println(wynik);
	}

}
