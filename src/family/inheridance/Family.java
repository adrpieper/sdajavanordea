package family.inheridance;

public class Family {

	public static void main(String[] args) {
		
		Mother mother = new Mother("Basia");
		Father father = new Father("Stanis�aw");
		Son son = new Son("Antoni");
		Daughter daughter = new Daughter("Celina");
		
		FamilyMember[] family = {father, mother, son, daughter};
		
		for (FamilyMember familyMember : family) {

			//Wywo�anie jest polimorficzne, 
			//typ obiektu decyduje, kr�ra metoda zostanie wywo�ana
			if (familyMember.isAdult()) {
				System.out.println("I'm an adult");
			}
			
			familyMember.introduce();
		}
	}
}
