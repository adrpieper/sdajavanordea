package family.inheridance;

public abstract class FamilyMember {
	private String name;
	private boolean isAdult;
	
	public FamilyMember(String name, boolean isAdult) {
		this.name = name;
		this.isAdult = isAdult;
	}
	
	public boolean isAdult() {
		return isAdult;
	}
	
	public String getName() {
		return name;
	}
	
	public abstract void introduce();
}
