package family.inheridance;

public class Son extends FamilyMember{

	public Son(String name) {
		super(name, false);
	}

	public void introduce(){
		System.out.println("Hi! I'm a son. My name is: " + this.getName());
	}
}
