package family.inheridance;


//Ojciec jest cz�onkiem rodziny
public class Father extends FamilyMember {

	public Father(String name){
		super(name, true); // Wywo�uje konstruktor z klasy FamilyMember
	}

	public void introduce(){
		System.out.println("Hi! I'm a father. My name is: " + this.getName());
	}
}
