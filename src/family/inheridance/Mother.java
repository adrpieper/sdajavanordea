package family.inheridance;

public class Mother extends FamilyMember {
	
	public Mother(String name) { // Parametry konstruktora Mother
		super(name, true); // parametry konstruktora FamilyMember
	}
	
	public void introduce(){
		System.out.println("Hi! I'm a mother. My name is: " + this.getName());
	}
}
