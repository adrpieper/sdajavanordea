package family.inheridance;

public class Daughter extends FamilyMember{

	public Daughter(String name) {
		super(name, false);
	}

	public void introduce(){
		System.out.println("Hi! I'm a daughter. My name is: " + this.getName());
	}
}
