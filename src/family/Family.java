package family;

public class Family {

	public static void main(String[] args) {
		
		Mother mother = new Mother("Basia");
		Father father = new Father("Stanis�aw");
		Son son = new Son("Antoni");
		Daughter daughter = new Daughter("Celina");
		
		// Wyb�r metody zale�y od typu referencji
		introduce(mother); 
		introduce(father);
		introduce(son);
		introduce(daughter);
	}
	
	// Cztery metody prezentuj�ce mechanizm przeci��ania
	public static void introduce(Mother mother){
		System.out.println("Hi! I'm a mother. My name is: " + mother.getName());
	}
	public static void introduce(Father father){
		System.out.println("Hi! I'm a father. My name is: " + father.getName());
	}
	public static void introduce(Son son){
		System.out.println("Hi! I'm a son. My name is: " + son.getName());
	}
	public static void introduce(Daughter daughter){
		System.out.println("Hi! I'm a daughter. My name is: " + daughter.getName());
	}
}
